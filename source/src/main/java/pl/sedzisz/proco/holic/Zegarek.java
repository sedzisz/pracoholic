package pl.sedzisz.proco.holic;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Zegarek extends Application {

	private static Integer hours = 8;
	private static Integer minutes = 0;
	private static Integer seconds = 0;

	private static final Logger log = LoggerFactory.getLogger(Zegarek.class);
	private final DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm:ss");

	public boolean IS_LINUX = System.getProperty("os.name").toLowerCase().contains("linux");

	private LocalTime upTime = LocalTime.now();
	private LocalTime endTime = LocalTime.now().plusHours(hours).plusMinutes(minutes).plusSeconds(seconds);

	private Timeline timeline;
	private static Alert alert;

	@Override
	public void start(final Stage primaryStage) throws Exception {
		if (IS_LINUX) {
			java.lang.System.setProperty("javafx.allowTransparentStage", "true");
		}
		primaryStage.initStyle(StageStyle.TRANSPARENT);

		setDefaultSize(primaryStage);
		primaryStage.setResizable(false);
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		primaryStage.setX(primaryScreenBounds.getMinX() + primaryScreenBounds.getWidth() - 100);
		primaryStage.setY(primaryScreenBounds.getMinY() + primaryScreenBounds.getHeight() - 145);
		primaryStage.show();
	}

	private void setDefaultSize(final Stage stage) {
		StackPane root = new StackPane();

		root.getChildren().add(addGridPane(stage));

		int maxWidth = 100;
		int maxHeight = 120;

		stage.setMinWidth(maxWidth);
		stage.setMinHeight(maxHeight);
		stage.setMaxWidth(maxWidth);
		stage.setMaxHeight(maxHeight);

		Scene scene = new Scene(root, maxWidth, maxHeight);
		scene.setFill(Color.TRANSPARENT);
		stage.setScene(scene);

		stage.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {
				if (e.getButton() == MouseButton.SECONDARY) {
					close();
				} else {
					moveWindow();
				}
			}
		});

	}

	public static void main(String[] args) {
		launch(args);
	}

	private void close() {
		log.info("Aplikacja zostanie zamknieta" + (areYouWorkTooLong() ? " po czasie !" : " przed czasem."));
		Platform.exit();
	}

	private void moveWindow() {
		log.info("No right click");
	}

	private void loger(LocalTime actualTime, LocalTime diffTime) {
		System.out.println("Uruchomiono o [" + upTime.format(format) + "] aktualny czas [" + actualTime.format(format)
				+ "] przepracowane [" + diffTime.format(format) + "]");
	}

	public GridPane addGridPane(final Stage primaryStage) {
		GridPane grid = new GridPane();
		grid.getChildren().clear();

		grid.setHgap(5);
		grid.setVgap(1);
		grid.setPadding(new Insets(0, 10, 0, 10));

		final Text textUpTime = new Text();
		textUpTime.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		grid.add(textUpTime, 0, 1);

		final Text textEndTime = new Text();
		textEndTime.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		grid.add(textEndTime, 0, 2);

		final Text textCurrentUpTime = new Text();
		textCurrentUpTime.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		textUpTime.setFill(Color.BLUE);
		grid.add(textCurrentUpTime, 0, 3);

		final Text textDiffTime = new Text();
		textDiffTime.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		grid.add(textDiffTime, 0, 4);

		final Text leftDiffTime = new Text();
		leftDiffTime.setFont(Font.font("Arial", FontWeight.BOLD, 20));
		grid.add(leftDiffTime, 0, 5);

		timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				LocalTime currentTime = LocalTime.now();
				LocalTime diff = diffFromUpTime(currentTime);
				LocalTime left = leftTimeToGoHomePolishPeople();

				textUpTime.setText(timeToString(upTime));
				textCurrentUpTime.setText(timeToString(currentTime));

				textEndTime.setText(timeToString(endTime));

				setGreenColorAfterTime(leftDiffTime);
				leftDiffTime.setText(timeToString(left));

				setRedColorAfterTime(textDiffTime);
				textDiffTime.setText(timeToString(diff));

				if (areYouWorkTooLong() && false) {
					createPopup(endTime);
				}

				primaryStage.setTitle(left.format(format) + "/" + timeToString(diff));

				log.debug("[" + timeToString(upTime) + "][" + timeToString(currentTime) + "][" + timeToString(diff)
						+ "][" + timeToString(left) + "]");
			}
		}));
		startAnimation();

		return grid;
	}

	private boolean areYouWorkTooLong() {
		return LocalTime.now().isAfter(endTime);
	}

	private void setRedColorAfterTime(Text text) {
		if (areYouWorkTooLong()) {
			text.setFill(Color.RED);
		} else {
			text.setFill(Color.BLACK);
		}
	}

	private void setGreenColorAfterTime(Text text) {
		if (areYouWorkTooLong()) {
			text.setFill(Color.GREEN);
		} else {
			text.setFill(Color.BLACK);
		}
	}

	private LocalTime diffFromUpTime(LocalTime actualTime) {
		return diff(upTime, actualTime);
	}

	private LocalTime diff(LocalTime start, LocalTime end) {
		LocalTime time = end.minus(start.getHour(), ChronoUnit.HOURS);
		time = time.minus(start.getMinute(), ChronoUnit.MINUTES);
		time = time.minus(start.getSecond(), ChronoUnit.SECONDS);
		time = time.minus(start.getNano(), ChronoUnit.NANOS);
		return time;
	}

	private LocalTime leftTimeToGoHomePolishPeople() {
		LocalTime time = endTime;
		if (!areYouWorkTooLong()) {
			time = time.minus(LocalTime.now().getHour(), ChronoUnit.HOURS);
			time = time.minus(LocalTime.now().getMinute(), ChronoUnit.MINUTES);
			time = time.minus(LocalTime.now().getSecond(), ChronoUnit.SECONDS);
			time = time.minus(LocalTime.now().getNano(), ChronoUnit.NANOS);
		} else {
			time = LocalTime.of(0, 0, 0);
			time = time.plus(LocalTime.now().getHour() - endTime.getHour(), ChronoUnit.HOURS);
			time = time.plus(LocalTime.now().getMinute() - endTime.getMinute(), ChronoUnit.MINUTES);
			time = time.plus(LocalTime.now().getSecond() - endTime.getSecond(), ChronoUnit.SECONDS);
			time = time.plus(LocalTime.now().getNano() - endTime.getNano(), ChronoUnit.NANOS);
		}
		return time;
	}

	private String timeToString(LocalTime time) {
		return time.format(format);
	}

	private void createPopup(LocalTime time) {
		if (!isActiveAlert()) {
			alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Powiadomienie");
			alert.setHeaderText(null);
			alert.setContentText("Upłynoł czas [" + timeToString(time) + "]");
			alert.show();

			if (alert.getResult() == ButtonType.OK) {
				timeline.play();
				close();
			}

			if (alert.getResult() == ButtonType.CANCEL) {
				timeline.play();
				endTime = endTime.plusSeconds(15);
				log.info("Powiadomienie zostalo przesuniete o 15 minut.");
				alert = null;
			}
		}
	}

	private void startAnimation() {
		timeline.setDelay(Duration.seconds(1));
		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
	}

	private void stopAnimation() {
		timeline.setDelay(Duration.seconds(1));
		timeline.setCycleCount(0);
		timeline.stop();
	}

	private boolean isActiveAlert() {
		return alert != null && alert.isShowing();
	}

}
