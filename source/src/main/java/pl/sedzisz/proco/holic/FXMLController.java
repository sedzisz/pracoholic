package pl.sedzisz.proco.holic;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class FXMLController implements Initializable {

    private int count = 0;

//    @FXML
//    private Button button;

    @FXML
    private Label label;

    private final DateFormat format = new SimpleDateFormat("HH:mm:ss");

    private static Timeline timeline;

    public FXMLController() throws ParseException {

        timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                final Calendar cal = Calendar.getInstance();
                label.setText(format.format(cal.getTime()));
            }
        }));

    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Niby zostal klikniety ale chuj z nim");
//        if (count++ == 2) {
//            System.exit(0);
//        }
    }

    @FXML
    private void onMouseClicked(ActionEvent event) {
        System.out.println("onMouseClicked");
    }

    @FXML
    private void onMouseDagged(ActionEvent event) {
        System.out.println("onMouseDagged");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        label.setStyle("-fx-background-color: transparent;");
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

        label.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        System.out.println("Double clicked");
                    }
                }
            }
        });
    }

}
